function logo() {
    location.href = "index.html";
}

function adopcion() {
    location.href = "adopcion_lista.html";
}

function ayudar() {
    location.href = "ayudar.html";
}

function noticias() {
    location.href = "noticias.html";
}

function contacto() {
    location.href = "contacto.html";
}

document.getElementById("buscar").onclick = function() {
    location.href = "busqueda.html";
};


if ('ontouchstart' in window) { var click = 'touchstart'; } else { var click = 'click'; }


$('div.header__menuMobile').on(click, function() {

    if (!$(this).hasClass('open')) { openMenu(); } else { closeMenu(); }

});

$('div.header__mobileClose').on(click, function() {

    closeMenu();

});


$('div.header__mobile ul li a').on(click, function(e) {
    e.preventDefault();
    closeMenu();
});


function openMenu() {

    $('div.header__mobile').addClass('expand');
    $('div.header__menuMobile').addClass('open');
    $('.header__mobile li').addClass('animate');

}

function closeMenu() {

    $('div.header__menuMobile').removeClass('open');
    $('div.header__mobile').removeClass('expand');
    $('.header__mobile li').removeClass('animate');

}